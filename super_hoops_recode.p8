pico-8 cartridge // http://www.pico-8.com
version 39
__lua__
--super hoops
--by stuart

function _init()
	load_menu_state()
	load_menu_defaults()
	load_tutorial_defaults()
	load_shop_defaults()
	load_game_state()

	ticks=0
end

function _update60()
	-- progress bar animation
	anim_frame=(anim_frame+1)%60
	
	if menu_state=="menu" then
		update_menu()
	elseif menu_state=="play" then
		update_game()
	elseif menu_state=="character" then
		update_character()
	elseif menu_state=="tutorial" then
		update_tutorial()
	elseif menu_state=="shop" then
		update_shop()
	end
end

function _draw()
	ticks+=1
	if menu_state=="menu" then
		draw_menu()
	elseif menu_state=="play" then
		draw_game()
	elseif menu_state=="character" then
		draw_character()
	elseif menu_state=="tutorial" then
		draw_tutorial()
	elseif menu_state=="shop" then
		draw_shop()
	end
	--print("state: "..menu_state,5,10,7)
	--print("ticks: "..ticks,5,20,7)
	--print("game: "..game_state,5,30,7)
end
-->8
-- updates
function update_menu()
	title:update()
	
	if btnp(⬆️) then
		if selected_item<=1 then
			selected_item=#menu_states
			return
		end
		selected_item-=1
	elseif btnp(⬇️) then
		if selected_item>=#menu_states then
			selected_item=1
			return
		end
		selected_item+=1
	elseif btnp(➡️) then
		update_menu_state(menu_states[selected_item])
	end
end

function update_character()
	if btnp(⬆️) then
		if selected_character_item<=1 then
			selected_item=#character_states
			return
		end
		selected_character_item-=1
	elseif btnp(⬇️) then
		if selected_character_item>=#character_states then
			selected_character_item=1
			return
		end
		selected_character_item+=1
	end
	
	if character_states[selected_character_item]=="character" then
		if btnp(➡️) then
			if character_index>=#characters then
				character_index=1
				return
			end
			character_index+=1	
		end
		if btnp(⬅️) then
			if character_index<=1 then
				character_index=#characters
				return
			end
			character_index-=1
		end
	else
		if btnp(➡️) then
			menu_state="menu"
			selected_character_item=1
		end
	end
end

function update_game()
			if game_state=="game_start" then
				if game_settings then
					if btnp(⬆️) then
				if selected_setting<=1 then
					selected_setting=#settings
					return
				end
				selected_setting-=1
			elseif btnp(⬇️) then
				if selected_setting>=#settings then
					selected_setting=1
					return
				end
				selected_setting+=1
			elseif btnp(➡️) then
				local item=settings[selected_setting]
				if item["name"]=="save" then
					game_settings=false
					return
				end
				if item["name"]=="back" then
					menu_state="menu"
					return
				end
				
				if item["value"]>=item["max_value"] then
					item["value"]=item["max_value"]
				else
					item["value"]+=1
				end
			elseif btnp(⬅️) then
				local item=settings[selected_setting]
				if item["name"]=="save" then
					return
				end
				if item["name"]=="back" then
					menu_state="menu"
					return
				end
				
				if item["value"]<=item["min_value"] then
					item["value"]=item["min_value"]
				else
					item["value"]-=1
				end
			end
			return
		end
		
		if progress_bar.progress>=progress_bar.max_value then
			sfx(2)
			reset_game()
			-- load settings
			team_1_size=settings[1]["value"]
			team_2_size=settings[2]["value"]
			game_playing_timer=settings[3]["value"]
			
			for i=1,team_1_size do
				add_player(20-i*5,112,1,1)
				add(team_1,players[#players])
			end
			for i=1,team_2_size do
				add_player(100-i*5,112,-1,2)
				add(team_2,players[#players])
			end
			
			for player in all(players) do
				player.ai=true
			end
			
			-- setup playable character
			players[1].ai=false
			players[1].sprite=characters[character_index]
			players[1].stamina=player_stamina
			players[1].power=player_power
			
			-- change state to start
			update_game_state("playing")
			progress_bar.progress=0
		else
			sfx(2,-2)
			progress_bar:update()
		end
	elseif game_state=="playing" then
		if game_playing_timer<=0 then
			gained_exp=false
			update_game_state("game_end")
		end
		
		for player in all(team_1) do
			player:update(h2)
		end
		for player in all(team_2) do
			player:update(h1)
		end
		
		if projectile_timer<=0 then
			if #projectiles<2 then
				add_projectile()
			end
			projectile_timer=60-((level*0.8)*10)
		else
			projectile_timer-=1
		end
		
		for t in all(trajectories) do
			t.life-=1
			if t.life<=0 then
				del(trajectories,t)
			end
		end
		
		for particle in all(particles) do
			if particle.timer>30 then
				del(particles,particle)
			else
				if particle.r>1 then
					particle.r-=0.1
				end
			end
			particle:update()
		end
		
		for alert in all(alerts) do
			alert:update()
		end
		
		for projectile in all(projectiles) do
			projectile:update(players[1])
		end
		
		ball:update()
		h1:update(2)
		h2:update(1)
		powerbar:update()
		game_playing_timer-=0.05
	elseif game_state=="game_end" then
		sfx(2,-2)
		clear_visual_effects()
		remove_players()
		exp_gained=2.5*team_1_score
		
		if #alerts<1 then
			if team_1_score>team_2_score then
				add_alert("you won!",64,20,60,true)
			elseif team_1_score<team_2_score then
				add_alert("you lost...",64,20,60,true)
			else
				add_alert("it's a draw!",64,20,60,true)
			end
		end
		
		for particle in all(particles) do
			if particle.timer>30 then
				del(particles,particle)
			else
				if particle.r>1 then
					particle.r-=0.1
				end
			end
			particle:update()
		end
		
		for alert in all(alerts) do
			alert:update()
		end
		
		if not gained_exp then	
			exp_bar:update(exp_gained)
			gained_exp=true
		end
		
		if btnp(❎) then
			update_game_state("game_start")
		end
		
		if btnp(🅾️) then
			menu_state="menu"
		end
		return
	end
end

function update_tutorial()
	if btnp(🅾️) then
		clear_visual_effects()
		update_stage()
		powerbar.enabled=false
	end
	
	if #team_1<1 then
		add_player(20,112,1,1)
		add(team_1,players[1])
	end
	if current_stage==13 then
		if #team_2<1 then
			add_player(100,112,-1,2)
			add(team_2,players[2])
		end
	end
	
	for player in all(players) do
		player.ai=true
	end
	players[1].ai=false
	players[1].sprite=characters[character_index]
	
	if current_stage==5 then
		if not powerbar.enabled then
			powerbar.enabled=true
			powerbar.value=flr(rnd(1*100))/100
			powerbar.speed=0.05
			powerbar.timer=9999
		end
		powerbar:update()
	else
		powerbar:update()
	end
	
	for player in all(team_1) do
		player:update(h2)
		if current_stage==2 or current_stage==7 then
			player.stats.stamina=20
		end
	end
	for player in all(team_2) do
		player:update(h1)
	end
	
	ball:update()
	if current_stage>8 then
	h1:update(2)
	end
	if current_stage>7 then
		h2:update(1)
	end
	if current_stage==13 then
		powerbar:update()
		game_playing_timer-=0.05
	end
	
	if current_stage==10 or current_stage==11 then
		example_game_time-=0.05
		if example_game_time<=0 then
			example_game_time=999
		end
	end
	
	for particle in all(particles) do
		if particle.timer>30 then
			del(particles,particle)
		else
			if particle.r>1 then
				particle.r-=0.1
			end
		end
		particle:update()
	end
		
	for alert in all(alerts) do
		alert:update()
	end
end

function update_shop()
	
end
-->8
-- drawing
function draw_menu()
	cls()
	map(0,0,0,0,128,128)
	title:draw()
	
	for i=1,#menu_states do
		if selected_item==i then
			shadow_text(menu_states[i],center_text_screen(menu_states[i]),15+20*(0.5+i),7)
		else
			print(menu_states[i],center_text_screen(menu_states[i]),15+20*(0.5+i),6)		
		end
	end
end

function draw_character()
	cls()
	map(48,0,0,0,128,128)
	
	for i=1,#character_states do
		local item=character_states[i]
		if selected_character_item==i then
			shadow_text(character_states[i],center_text(item,32),20*i,7)
			
			if item=="character" then
				local center_x=center_text("character: "..character_index,96)
				print("character: "..character_index,center_x,20,7)
			end
		else
			print(item,center_text(item,32),20*i,6)		
		end
		spr(characters[character_index],screen_width()/2-8+36,80)
	end
end

function draw_game()
	cls()
	if game_state=="game_start" then
		if game_settings then
			map(16,0,0,0,128,128)
			for i=1,#settings do
				if selected_setting==i then
					local item=settings[i]
					shadow_text(item["name"],center_text(item["name"],32),20*i,7)
					print(item["tip"],70,40,7)
					if item["value"]!=nil then
						print("<",70,50,13)
						print(item["value"],76,50,12)
						print(">",76+(#tostr(item["value"])*6)-(#tostr(item["value"])),50,13)			
					end
				else				
					print(settings[i]["name"],center_text(settings[i]["name"],32),20*i,6)
				end
			end
			return
		end
		map(32,0,0,0,128,128)
		
		shadow_text("loading level",center_text_screen("loading level"),60,7)
		print(ceil(progress_bar.progress*100).."%",center_text_screen(tostr(ceil(progress_bar.progress*100)).."%"),70,7)
		progress_bar:draw()
	elseif game_state=="playing" then
		get_map(level)
		
		for player in all(players) do
			player:draw()
		end
		
		for i=1,#trajectories-1 do
			local p1=trajectories[i]
			local p2=trajectories[i+1]
			if i%2!=0 then 
				line(p1.x,p1.y,p2.x,p2.y,2)
			else
				rectfill(p1.x-1,p1.y-1,p1.x+1,p1.y+1,8)
			end
		end
		
		for particle in all(particles) do
			particle:draw()
		end
		
		for projectile in all(projectiles) do
			projectile:draw()
		end
		
		ball:draw()
		h1:draw()
		h2:draw()
		powerbar:draw()
		add_stamina_bar(players[1])
		main_game_ui()
		
		for alert in all(alerts) do
			alert:draw()
		end
	elseif game_state=="game_end" then
		for particle in all(particles) do
			particle:draw()
		end
		
		for alert in all(alerts) do
			alert:draw()
		end
		
		exp_bar:draw()
		
		local quit_text="press z to quit"
		local restart_text="press x to restart"
		print(quit_text,center_text_screen(quit_text),60,7)
		print(restart_text,center_text_screen(restart_text),68,7)
		
		return
	end
end

function draw_tutorial()
	cls()
	get_map(2)
	local continue_text="press z to continue..."
	local exit_text="press z to exit the tutorial"
	
	if current_stage==1 then
		if #alerts<1 then
			add_alert("welcome to super hoops!",64,30,60,true)
		end
		print(continue_text,center_text_screen(continue_text),50,7)
	
	elseif current_stage==2 then
		if #alerts<1 then
			add_alert("use '<' and '>' to move",64,64,60,true)
		end
		print(continue_text,center_text_screen(continue_text),84,7)

		for player in all(players) do
			player:draw()
		end
	
	elseif current_stage==3 then
		if #alerts<1 then
			add_alert("you lose stamina when moving! you can upgrade it at the shop",64,30,60,true)
		end
		print(continue_text,center_text_screen(continue_text),50,7)
		
		for player in all(players) do
			player:draw()
		end
		add_stamina_bar(players[1])
		
	elseif current_stage==4 then
		if #alerts<1 then
			add_alert("use x to shoot the ball...",64,40,60,true)
		end
		print(continue_text,center_text_screen(continue_text),60,7)
	
	elseif current_stage==5 then
		if #alerts<1 then
			add_alert("a powerbar will appear...",64,80,60,true)
		end
		print(continue_text,center_text_screen(continue_text),100,7)
		powerbar:draw()
		
	elseif current_stage==6 then
		if #alerts<1 then
			add_alert("you will lose the ball if you take too long!",64,30,60,true)
		end
		print(continue_text,center_text_screen(continue_text),50,7)
	
	elseif current_stage==7 then
		if #alerts<1 then
			add_alert("have a go at shooting...",64,70,60,true)
		end
		print(continue_text,center_text_screen(continue_text),90,7)
		
		for player in all(players) do
			player:draw()
		end
		ball:draw()
		powerbar:draw()
		
	elseif current_stage==8 then
		if #alerts<1 then
			add_alert("getting the ball in the hoop will increase your teams score",64,60,60,true)
		end
		print(continue_text,center_text_screen(continue_text),90,7)
		
		for player in all(players) do
			player:draw()
		end
		ball:draw()
		h2:draw()
		powerbar:draw()
		add_stamina_bar(players[1])
		
	elseif current_stage==9 then
		if #alerts<1 then
			add_alert("watchout you dont end up scoring in your own hoop...",64,30,60,true)
		end
		print(continue_text,center_text_screen(continue_text),60,7)
	
	elseif current_stage==10 then
		if #alerts<1 then
			add_alert("there is a game timer that constantly ticks down",50,25,60,true)
		end
		print(continue_text,center_text_screen(continue_text),55,7)
		
		local game_time="time:"..ceil(example_game_time)
		print(game_time,5,5,7)
	
	elseif current_stage==11 then
		if #alerts<1 then
			add_alert("when the timer runs out or a team scores a certain amount of goals, the game will end",64,25,60,true)
		end
		print(continue_text,center_text_screen(continue_text),65,7)
		
		local game_time="time:"..ceil(example_game_time)
		print(game_time,5,5,7)
		
	elseif current_stage==12 then
		if #alerts<1 then
			add_alert("thats all the information i have for now. why not put your skills to the test in a practice match?",64,25,60,true)
		end
		print(continue_text,center_text_screen(continue_text),70,7)
	
	elseif current_stage==13 then
		print(exit_text,center_text_screen(exit_text),20,7)
		
		for player in all(players) do
			player:draw()
		end
		
		ball:draw()
		h1:draw()
		h2:draw()
		powerbar:draw()
		add_stamina_bar(players[1])
		main_game_ui()
		
	elseif current_stage==14 then
		menu_state="menu"
	
	end
	
	for particle in all(particles) do
		particle:draw()
	end
	
	for alert in all(alerts) do
		alert:draw()
	end
end

function draw_shop()
	cls()
end
-->8
-- game state
function load_game_state()
	game_state="game_start"
	game_states={"game_start","playing","game_end"}

	game_playing_timer=300
	game_end_timer=60
	
	game_settings=true
	selected_setting=1
	settings={
		{name="team 1",tip="team 1 size",value=1,min_value=1,max_value=2},
		{name="team 2",tip="team 2 size",value=1,min_value=0,max_value=2},
		{name="timer",tip="game time",value=300,min_value=100,max_value=1000},
		{name="save",tip="start game"},
		{name="back",tip="go to menu"}
	}
	
	team_1_size=1
	team_1_score=0
	team_2_size=1
	team_2_score=0
	max_team_size=2
	
	team_1={}
	team_2={}
	players={}
	ball=add_ball(60,60,3)
	h1=add_hoop(8,80,4)
	h2=add_hoop(112,80,5)
	powerbar=add_powerbar()
	level=1
	exp=0
	max_exp=60+(level*2.5)
	gained_exp=false
	exp_bar=add_exp_bar()
	regen_timer=0
	
	trajectories={}
	projectiles={}
	projectile_timer=60
	projectile_types={"lose_point","release"}
	particles={}
	alerts={}
	
	anim_frame=0
	progress_bar=add_progress_bar((128/2)-60+30,80,1)
end

function update_game_state(state)
	for _state in all(game_states) do
		if _state==state then
			game_state=_state
		end
	end
end

function reset_game()
	reset_ball()
	clear_visual_effects()
	remove_players()
	team_1_score=0
	team_2_score=0
	game_playing_timer=300
end

function get_map(l)
	if l==1 then
		map(0,16,0,0,128,128)
	elseif l==2 then
		map(16,16,0,0,128,128)
	elseif l==3 then
		map(32,16,0,0,128,128)
	elseif l==4 then
		map(48,16,0,0,128,128)
	else
		get_map(1)
	end
end
-->8
-- menu state
function load_menu_state()
	menu_state="menu"
	menu_states={"play","character","tutorial","shop"}
end

function update_menu_state(state)
	for _state in all(menu_states) do
		if _state==state then
			menu_state=_state
		end
	end
end

function load_menu_defaults()
	sfx(2)
	
	-- need to implement saving/loading game
	cartdata("stuart_superhoops_1")
	
	subtitles={"goty edition", "play minecraft","7-0","6 times!","steven klopp","michael jordan!"}
	title=title_screen_text("super hoops!",rnd(subtitles))
	
	selected_item=1
	
	character_states={"character","apply"}
	selected_character_item=1
	characters={1,64,65,66,67,68,69,70,71,72,73,74,75,76,77}
	character_index=1
	selected_character=characters[1]
end

-->8
-- sprites
function add_player(x,y,direction,sprite_id)
	add(players,{
		x=x,
		y=y,
		vx=0,
		vy=0,
		direction=direction,
		sprite=sprite_id,
		score=0,
		speed=2,
		stats={
			stamina=20,
			power=20,
			regeneration=20
		},
		ai=false,
		update=function(self,h)
			if self.ai then
				move_ai(self,h)
				return
			end
			
			if btn(➡️) then
				move_player(self,self.speed,0)
			end
			if btn(⬅️) then
				move_player(self,-self.speed,0)
			end
			if btnp(❎) then
				if not is_holding(self,ball) then return end
				
				if powerbar.enabled then
					powerbar.enabled=false
					powerbar.timer=0
					ball.power=powerbar.value*ball.max_power
					sfx(0)
					throw_ball(self,ball)
				else
					powerbar.enabled=true
					powerbar.value=flr(rnd(1*100))/100
					powerbar.speed=0.05
					powerbar.timer=50
					ball.power=0
				end
			end
			get_ball(self,ball)
			
			if self.stats.stamina<player_stamina then
				if regen_timer>=120-self.stats.regeneration then
					regen_timer=0
					self.stats.stamina+=self.stats.regeneration/10
				else
					regen_timer+=1
				end
			end
		end,
		draw=function(self)
			spr(self.sprite,self.x,self.y)
		end
	})
end

function remove_players()
	for player in all(players) do
		del(players,player)
	end
end

function add_ball(x,y,sprite_id)
	return {
		x=x,
		y=y,
		vx=0,
		vy=0,
		power=0,
		max_power=60,
		speed=2,
		sprite=sprite_id,
		held_by=nil,
		update=function(self)
			if self.held_by!=nil then
				if self.held_by.direction>0 then
					self.x=self.held_by.x+8
				else
					self.x=self.held_by.x-8
				end
				self.y=self.held_by.y
				return
			end
			
			self.x=self.x+self.vx
			self.y=self.y+self.vy
			self.vy=self.vy+0.1
			
			--bounce
			if self.x>112 then
				self.x=112
				self.vx=-self.vx*0.8
			elseif self.x<8 then
				self.x=8
				self.vx=-self.vx*0.8
			end
			if self.y>112 then
				self.y=112
				self.vy=-self.vy*0.8
			elseif self.y<8 then
				self.vy=-self.vy
			end
			
			if (ticks%5==0) then
				add_trajectory(self.x+4,self.y+4)
			end
		end,
		draw=function(self)
			spr(self.sprite,self.x,self.y)
		end
	}
end

function add_hoop(x,y,sprite_id)
	return {
		x=x,
		y=y,
		sprite=sprite_id,
		update=function(self,team)
			if colliding(self.x,self.y,ball.x,ball.y) then
				add_alert("scored!",64,30,60,true)
		
				if team==1 then
					team_1_score+=1
				else
					team_2_score+=1
				end
				sfx(1)
				reset_ball(ball)
			end
		end,
		draw=function(self)
			spr(self.sprite,self.x,self.y)
		end
	}
end

function add_projectile()
	local ptype=rnd(projectile_types)
	add(projectiles,{
		x=flr(rnd(112))+8,
		y=-10,
		timer=30,
		update=function(self,p)
			self.y+=1
			if self.y>128 then
				del(projectiles,self)
			end
			
			if colliding(self.x,self.y,p.x,p.y) then
				del(projectiles,self)
				if ptype=="lose_point" then
					if team_1_score>0 then
						team_1_score-=1
					end
				elseif ptype=="release" then
					throw_ball(p,ball)
				end
			end
		end,
		draw=function(self)
			if ptype=="lose_point" then
				spr(7,self.x,self.y)
			elseif ptype=="release" then
				spr(6,self.x,self.y)
			end
		end
	})
end
-->8
-- movement
function move_player(p,dx,dy)
	if p.stats.stamina<=0 then return end
	
	p.vx=dx
	p.vy=dy
	p.x=p.x+p.vx
	p.y=p.y+p.vy
	
	p.x=mid(p.x,8,112)
	p.y=mid(p.y,0,112)
	
	if dx~=0 then
		p.direction=dx>0 and 1 or -1
	end
	
	p.stats.stamina-=0.5
end

function move_ai(p,h)
	local dx=ball.x-p.x
	local dy=ball.y-p.y
	local dist=sqrt(dx*dx+dy*dy)
	
	get_ball(p,ball)
	
	if dist>10 then
		p.x+=dx/dist*p.speed
	end
	
	if ball.held_by==p then
		local hx=h.x-p.x
		local hy=h.y-p.y
		local hdist=sqrt(hx*hx+hy*hy)
		
		if hdist>p.speed then
			p.x+=hx/hdist*p.speed
		end
		
		throw_ball(p,ball)
	end
end
-->8
-- collisions
function colliding(x1,y1,x2,y2)
	local dx=x1-x2
	local dy=y1-y2
	local dist=sqrt(dx*dx+dy*dy)
	return dist<8
end
-->8
-- ball physics
function reset_ball(b)
	ball.x=flr(rnd(90))+20
	ball.y=60
	ball.vx=0
	ball.vy=0
	ball.held_by=nil
end

function is_holding(p,b)
	return b.held_by==p
end

function get_ball(p,b)
	if is_holding(p,b) then return end
	if b.held_by==nil and colliding(p.x,p.y,b.x,b.y) then
		b.held_by=p
		b.vx=0
		b.vy=0
	end
end

function throw_ball(p,b)
	if is_holding(p,b) then
		b.held_by=nil
		if p.ai then
			if p.direction>0 then
				b.vx+=b.speed*abs(b.max_power/b.max_power)
			else
				b.vx-=b.speed*abs(b.max_power/b.max_power)
			end
		else
			if p.direction>0 then
				b.vx+=b.speed*abs(b.power/b.max_power)
			else
				b.vx-=b.speed*abs(b.power/b.max_power)
			end
		end
	end
	if p.ai then
		b.vy-=4*abs(b.max_power/b.max_power)
	else
		b.vy-=4*abs(b.power/b.max_power)
	end
end
-->8
-- ui
function screen_width()
	return 128
end

function screen_height()
	return 128
end

function center_text_screen(text)
	return screen_width()/2-(#text*2)
end

function center_text(text,x)
	local text_w=#text*4
	return x-text_w/2
end

function shadow_text(text,x,y,col)
	print(text,x,y+1,0)
	print(text,x,y,col)
end

function title_screen_text(text,subtitle)
	return {
		text=text,
		subtitle=subtitle,
		x=0,
		y=15,
		speed=0.5,
		bounce=0,
		update=function(self)
			self.x=self.x+self.speed
			if self.x>35 then
				self.x=35
				self.bounce+=1
				self.speed=-self.speed
			elseif self.x<0 then
				self.x=0
				self.bounce+=1
				self.speed=-self.speed
			end
		end,
		draw=function(self)
			local offset=self.bounce*2
			for i=1,#self.text do
				local char=sub(self.text,i,i)
				print(char,self.x+(i-1)*8,self.y+1,0)
				print(char,self.x+(i-1)*8,self.y,9)
			end
			print(self.subtitle,center_text_screen(self.subtitle),self.y+9,0)
			print(self.subtitle,center_text_screen(self.subtitle),self.y+8,10)
		end
	}
end

function main_game_ui()
	--scores
	print(team_1_score,center_text_screen(tostr(team_1_score))-10,5,7)
	print(team_2_score,center_text_screen(tostr(team_2_score))+10,5,7)
	
	--time
	local game_time="time:"..ceil(game_playing_timer)
	print(game_time,5,5,7)
end

function add_exp_bar()
	return {
		g_xp=0,
		update=function(self,g_xp)
			self.g_xp=g_xp
			exp+=self.g_xp
			if exp>=max_exp then
				level+=1
				exp-=max_exp
				max_exp*=2
			end
		end,
		draw=function(self)
			sfx(2,-2)
			local bar_w=80
			local bar_h=8
			local bar_x=64-(bar_w/2)
			local bar_y=112
			
			rect(bar_x,bar_y,bar_x+bar_w,bar_y+bar_h,7)
			
			local exp_percent=exp/max_exp
			rectfill(bar_x+1,bar_y+1,bar_x+bar_w*exp_percent,bar_y+bar_h-1,10)
			
			local level_text="level:"..level.." exp:"..exp.."/"..max_exp
			print(level_text,center_text_screen(level_text),bar_y-8,7)
			
			if not gained_exp then
				for i=1,bar_w do
					rectfill(bar_x+i,bar_y+1,bar_x+i,bar_y+bar_h-1,10)
					flip()
					sfx(4)
				end
			end
		end
	}
end

function add_powerbar()
	return {
		enabled=false,
		value=0.0,
		speed=0.05,
		value_max=60,
		timer=50,
		update=function(self)
			if self.enabled then
				self.timer-=1
				self.value+=self.speed
				if self.timer<=0 then
					self.enabled=false
					return
				end
				
				if self.value>1.0 then
					self.value=1.0
					self.speed=-self.speed
				elseif self.value<0.0 then
				 self.value=0.0
				 self.speed=-self.speed
				end
			end
		end,
		draw=function(self)
			if not self.enabled then return end
			
			print("time left: " .. self.timer/10,screen_width()/2-42,45,7)
			-- draw powerbar background
	  rectfill(screen_width()/2-45, 20, screen_width()/2+45, 40, 0)
	  rectfill(screen_width()/2-42, 23, screen_width()/2+42, 37, 7)
	  rectfill(screen_width()/2-40, 25, screen_width()/2+40, 35, 0)
	
	  -- draw powerbar slider
	  local slider_x = screen_width()/2-42 + 84 * self.value
	  rectfill(screen_width()/2-40, 25, slider_x+4, 35, 8)
			rectfill(slider_x-4, 20, slider_x+4, 40, 7)
	  rectfill(slider_x-2, 18, slider_x+2, 22, 7)
	  rectfill(slider_x-2, 42, slider_x+2, 38, 7)
		end
	}
end

function add_stamina_bar(p)
	local bar_w=15
	local bar_h=3
	local bar_x=p.x-(bar_w/2)+4
	local bar_y=p.y+10
	local bar_pad=1
	
	local stamina_ratio=p.stats.stamina/player_stamina
	local fill_w=(bar_w-2*bar_pad)*stamina_ratio
	
	rectfill(bar_x,bar_y,bar_x+bar_w,bar_y+bar_h,0)
	rectfill(bar_x+bar_pad,bar_y+bar_pad,bar_x+bar_pad+fill_w,bar_y+bar_h-bar_pad,12)
end

function add_progress_bar(x,y,max_value)
	return {
		enabled=false,
		x=x,
		y=y,
		width=60,
		height=10,
		progress=0,
		max_value=max_value,
		update=function(self)
			if self.progress<self.max_value then
				self.progress+=0.01
			end
		end,
		draw=function(self)
			rectfill(self.x,self.y,self.x+self.width,self.y+self.height,7)
			rectfill(self.x+1,self.y+1,self.x+self.width-1,self.y+self.height-1,1)
			
			local fill_width=self.width*self.progress
			rectfill(self.x+1,self.y+1,self.x+fill_width-1,self.y+self.height-1,14)
			
			anim_frame=(anim_frame+1)%40
			if anim_frame<20 then
				line(self.x+fill_width,self.y+1,self.x+fill_width,self.y+self.height-1,7)
			else
				line(self.x+fill_width,self.y+1,self.x+fill_width,self.y+self.height-1,0)
			end
		end
	}
end

function add_trajectory(x,y)
	add(trajectories,{
		x=x,
		y=y,
		life=20
	})
end

function add_particle(x,y)
	add(particles,{
		x=x,
		y=y,
		vx=rnd(2)-1,
		vy=rnd(2)-1,
		r=rnd(4),
		col=2+rnd(14),
		timer=0,
		update=function(self)
			self.timer+=1
			self.x+=self.vx*2
			self.y+=self.vy*2
			
			if self.x>=120 then
				self.x=120
			elseif self.x<=8 then
				self.x=8
			elseif self.y>=120 then
				self.y=120
			elseif self.y<=8 then
				self.y=8
			end
		end,
		draw=function(self)
			circfill(self.x,self.y,self.r,self.col)
		end
	})
end

function add_alert(text,x,y,timer,particles)
	add(alerts,{
		x=x,
		y=y,
		timer=timer,
		update=function(self)
			self.timer-=1
			if self.timer<=0 then
				del(alerts,self)
			end
		end,
		draw=function(self)
			local lines=split_text(text,200)
			local temp_y=0
			local text_h=8
			local pad=text_h/2
			local rect_x=center_text(lines[longest_str(lines)],self.x)-2
			local rect_y=self.y-pad
			local rect_w=#lines[longest_str(lines)]*pad+5
			local rect_h=(text_h*#lines)+pad
			
			if particles then
				add_particle(rect_x+rect_w/2,rect_y+rect_h/2)
			end
			
			rectfill(rect_x,rect_y,rect_x+rect_w,rect_y+rect_h,0)
			rectfill(rect_x+2,rect_y+2,rect_x+rect_w-2,rect_y+rect_h-2,8)
			
			for i=1,#lines do
				local curr_line=lines[i]
				print(curr_line,center_text(curr_line,self.x),self.y+temp_y,7)
				temp_y+=text_h
			end
			--print(text,center_text(text,self.x,self.y),self.y,7) 
		end
	})
end

function clear_visual_effects()
	for particle in all(particles) do
		del(particles,particle)
	end
	
	for alert in all(alerts) do
		del(alerts,alert)
	end
	
	for t in all(trajectories) do
		del(trajectories,t)
	end
	
	for p in all(projectiles) do
		del(projectiles,p)
	end
end

function longest_str(lines)
	local curr_len=0
	local curr_index=1
	local highest_index=1
	for l in all(lines) do
		if str_px_width(l)>curr_len then
			curr_len=str_px_width(l)
			highest_index=curr_index
		end
		curr_index+=1
	end
	return highest_index
end

function str_px_width(str)
	local width=0
	
	for i=1,#str do
		local c=sub(str,i,i)
		width=width+8
	end
	return width
end

function split_text(text,max_width)
	local words={}
	for word in all(split(text," ")) do
		add(words,word)
	end
	
	local lines={}
	local curr_line=""
	for i=1,#words do
		local word=words[i]
		local word_width=str_px_width(word)
		if(str_px_width(curr_line.." "..word)>max_width) then
			add(lines,curr_line)
			curr_line=word
		else
			curr_line=curr_line.." "..word
		end
	end
	add(lines,curr_line)
	return lines
end
-->8
-- shop state
function load_shop_defaults()
	player_power=20
	player_stamina=20
end
-->8
-- tutorial state
function load_tutorial_defaults()
	current_stage=1
	max_stages=40
	example_game_time=999
end

function update_stage()
	if current_stage<max_stages then
		current_stage+=1
	end
end
__gfx__
000000000044440000555500005995000088880000999900000a9000000dd0000000000000000000000000000000000000000000000000000000000000000000
0000000000f7f70000747400099959900870078009700790000aa0000001d0000000000000000000000000000000000000000000000000000000000000000000
0070070000ff0f000040440059959995807007089070070900a9aa0000c1dd000000000000000000000000000000000000000000000000000000000000000000
00077000fccccccf466666649595995978800887799009970a999aa00c1dddd00000000000000000000000000000000000000000000000000000000000000000
00077000f00cc00f4006600495959959707887077079970799999999cc11d1110000000000000000000000000000000000000000000000000000000000000000
007007000077770000777700599599957070070770700707049999400cc111c00000000000000000000000000000000000000000000000000000000000000000
0000000000f00f00004004000999599007700770077007700044940000cc1c000000000000000000000000000000000000000000000000000000000000000000
000000000cc00cc00770077000599500007777000077770000044000000cc0000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
007000700000000000eeea00000aaa9000eaaa900ccccc00099999900888880000aaaa000cccc10000a9a9a0000a9a0000000000000010000000000000000000
00777770009999900eeaaaa000aa0a090eaa0a09ccc7ccc00979990988e888800aaaaaa0ccccaa1000aaaaa000aaaaa0000acca0001111110000000000000000
0070ff00094799470eee0f0000aa0a0900aa0a090cccc0c000990900828880800aaa0a00ccca0a0000aa0a000aaa0a0a00aaaaaa1111f1100000000000000000
bb7ffff0099490940eeefff00aaaaaa90aaaaaa9cccdfff4009666618282fff10afffff00caaaaa000aaaaa000aaaaa0009aa0a001ff0f000000000000000000
3cbcccc0049449440eee88e099aa880999aaaea901cc4400d409dd00827644000affff40000aaa0000aaaa00000aa90000aaaa88201fff000000000000000000
f3ccccc4099999900eeeffe0990aaa92990aaa920c1cff1d0494664d0876ff5d077777f00aaaa9900888882000a882900009aa88229940f00000000000000000
00111110090999940eee22e000880220002e0220070c0100d2740400000b035daacccc19a0bbb30990cccc190a88822900ccc11009f114400000000000000000
00f0004000090040008888000888022200ee022200870d20000862d00089042000cc01100088220000cc0110000994000cccac19011000110000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4444444477777777666666666666666666666666555555555577775555aaaa555599995555666655cc76ccccccccccccccccccccccaaaacccccccccccccccccc
444444447777777766666666666666666666666655555555577777755aaaaaa55999999556666665ccc76ccccccccccccccccccccaaaaaaccccccccccccccccc
44444444777777776666666666666666666666665555555577777777aaaaaaaa99999999666666666ccc76ccccccccccccccccccaaaaaaaacccc777ccccc77cc
44444444777777776666666666666666666666665555555577777777aaaaaaaa999999996666666676ccc76cccccccccc33333ccaaaaaaaacc777777cc77777c
44444444777777776666666666666666666666665555555577777777aaaaaaaa9999999966666666c76ccc76ccccccccc333333caaaaaaaacc777777c777777c
44444444777777775555555555555555555555555555555577777777aaaaaaaa9999999966666666cc76cccccccccccccc33333caaaaaaaaccc7777ccc7777cc
44444444777777775555555555555555dddddddd55555555577777755aaaaaa55999999556666665ccc76cccccccccccc3333333caaaaaaccccccccccccccccc
444444447777777755555555dddddddddddddddd555555555577775555aaaa555599995555666655cccc76cccccccccc33333333ccaaaacccccccccccccccccc
117611111111111111111111117777111111111111111111bbbbbbbbbbbbb3bbbbbbbbbbcc5995ccffffffffdddddddd11111111111111113333333333333333
111761111111111111111111177777711111111111111111bbbbbbbbbbbb3bbbbbbb7bbbc999599cffffffffdddddddd1711111111111111333333333333b333
611176111111111111111111777777771111666111116611bbbbbbbbb3bb3bbbbb37a7bb59959995ffffffffdddddddd1111111111111111333333333b333b33
761117611111111113333311777777771166666611666661bbbbbbbbbb3bbb3bbbb37bbb95959959ffffffffdddddddd11111111111111113333333333b33b33
176111761111111113333331777777771166666616666661bbbbbbbbbb3bb3bbbbb3bbbb95959959ffffffffdddddddd11117111111111113333333333b3333b
117611111111111111333331777777771116666111666611bbbbbbbbbb3bb3bbbbb33bbb59959995ffffffffdddddddd11111111111171113333333333b333b3
111761111111111113333333177777711111111111111111bbbbbbbbbbbbbbbbbbbb3bbbc999599cffffffffdddddddd111111111111111133333333333333b3
111176111111111133333333117777111111111111111111bbbbbbbbbbbbbbbbbbbbbbbbcc5995ccffffffffdddddddd11111111111111113333333333333333
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666666668888888899999999aaaaaaaa222222220000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__gff__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1dccbcbcbcbcbcbc5c5c5c5c6c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d3d1d1d5cbcbcecbcbcbc5c7c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1dcd1d1d1d1cecbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d4d1ddd1cbcbcbcdcbcfc9c5c5c5c8c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1dcd1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4dbdbe4e4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc1dbdbdbdbdbdbc1d4d1ddd1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4dbdbdbdbe4e4c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcccbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d1d4d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4dbdbe4e4e4e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcccbcbcbcbcbcbcbcbcbdedecbcbcbc1dbdbdbdbdbdbc1d1d1d3d1d1d1d1d1d1d1d1d1d1cbcbcbcbcbcbc5c5c5c5c5c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2e2e2e2e2c0c0c0c0c0c0e2e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
dedecbcbcbcbcbcbcbccdededecbcbcbc1dbdbdbdbdbdbc1d1d1d1d1d1d5d1d1d1d1d1d1d1cbcbcbcbcbcbc4c3c3c4c3c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2e2c0c0c0c0c0c0c0c0c0c0e2e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
dededecccbd9cbcbcbdededededecbcbc1dbdbdbdbdbdbc1dcd1d1d1d1d1d1ddd1d1d1d1d1cbcbcbcbcbcbc3c4c3c2c2c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2e2c0c0c0c0c0c0c0c0c0c0c0c0e2e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
d7d7d6d6d6d8d6d6d6d7d6d6d7d6d8d6c1dbdbdbdbdbdbc1d1d1d1d1d1d1d1d1dedfdededed6d6d6d7d8d6c0c0c0c0c0c1dbdbdbdbdbdbc1e4e4e4e4e4e4e4e4e2c0c0c0c0c0c0c0c0c0c0c0c0c0c0e2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbd1d1d1d1d1d1ddd1d1d1d1d1d1d1d1d1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5d1d1d4d1d1d1d1d1d1d1d1d5d1d1d1d100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcecbcbcbcbcecbcbcbcbcbd1d1d1d1d1d4dcd1d1d1d4d1d1d1dcd1c9c5c5c5c5c5c5c5c6c5c5c5c5c5c6c5d1d1d1d1d1d1c5c5c5d1d1d1d1d1d1d100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cfcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbd5d1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c5c5c5c7c5c5c5c5c5c5c7c5c5c5c5c5d1d1d1d1c5c5c5c5c5c5c5c5d1d4d1d100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbd1d1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5d5d1d1c5c5e0e0c1c1dbdbc5c5c5d1d100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcdcbcbcbcfd1d1d1d1d1d1d1d1d1d1dcd3d1d1d1d5c5c6c5c5c5c5c5c6c5c5c5c9c5c5c7c5d1d1c5c5e0c1dbdbdbdbdbdbe0c5c5d100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcecbcbcbcbcbcbcbcbcbcbcbd1d1d1d1d4d1d1d1d1d1d1d1d1ddd1d1c5c5c5c8c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5e0dbdbdbdbdbdbdbdbdbc1c5c500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbd1d1d1d1d1d1d1d1dcddd1d1d1d1ddd1c5c5c5c5c5c5c5c5c5c5c5c5c8c5c5c5c5e0c1dbdbdbdbdbdbdbdbdbdbdbdbdb00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
cbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbd1d1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c5dbdbdbdbdbdbdbe3e3dbdbdbdbdbdbdb00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cbcbcbcbcbcbcbcbcbcbcecbcbcbc1c1d1d1d1d1d1d1d1d1d1d1d4d1d1d1c1c1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c1c1dbdbdbdbdbe3e3e2e3dbdbdbdbdbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cbcbcbcbcbcbcbcbcbcbcbcbcbcbc1c1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c1c1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c1c1dbdbdbdbdbe3e2e2e2dbdbdbdbdbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cbcbcbcbcbcbcbcbcbcbcbcbcbcbc1c1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c1c1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c1c1dbdbdbdbe2e2e2e2e2e3dbdbdbdbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cbcbcbcbcbcbcbcbcbcbcbcbcbcbc1c1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c1c1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c1c1dbdbdbe2e2e1e1e1e2e2e3e3dbdbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cacacacacacacacacacacacacacac1c1d0d0d0d0d0d0d0d0d0d0d0d0d0d0c1c1c3c2c4c3c2c2c3c2c2c4c2c2c3c4c1c1dbdbe2e1e1e1e4e1e1e2e2e2dbdbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cacacacacacacacacacacacacacac1c1d0d0d0d0d0d0d0d0d0d0d0d0d0d0c1c1c3c3c2c2c4c2c2c4c3c2c4c2c2c2c1c1dbe1e1e1e4e4e4e4e1e4e1e1e2dbc100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c1cbcbcbcbcbcbcbcbcbcbcbcbcbcbc1c1d1d1d1d1d1d1d1d1d1d1d1d1d1d1c1c1c2c3c4c3c2c2c2c4c3c3c2c4c2c2c1c1e1e1e4e4e4e4e4e4e4e4e4e4e1e1c100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
c5c5c5c5c5c5c5c1c1c5c5c5c5c5c5c5c5c5c5c5c5c5c5c1c1c5c5c5c5c5c5c5c0c0c0c0c0c0c0c1c1c0c0c0c0c0c0c0c0c0c0c0c0c0c0c1c1c0c0c0c0c0c0c000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
000100000b7501175014750177501c7501f7502175023750257502575023750227501f7501875013750107500c750097700775005750017500075026700107000170000000000000000000000000000000000000
0002000000000000001265015650166501a6501c6501d6501e05020050220502505027050280501e3501b3501835017350275501d4501d4501c4501b450194502a7502c7502e7503075031750000000000000000
002300080e5300b53003050060500d55007150160500b550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0010001f1b7500c5501a750000000c5501a750000000c5501975000000000001a7501975017750187500d5501775000000187500d5501775000000000000000015750000000c5501775000000000000000000000
0010000005750287500c7501870000000000000000011700000000000000000000002d2001b100237000970018700000000000000000000000000000000000000000000000000000000000000000000000000000
001b000025550245502255021550205501e5501c5501a550185501655014550115500f5500e5500d5500b5500a550095500955009550095500955008550075500655005550055500455003550015500055000550
00030000107500d75023750135500a7500835027050145501d750121500a750115502d7500d350270501215014750255502e75018350247502b150345503175036750221502d050105500e5500c5500b55000000
00100000000000000000000263500000000000112501835000000000001e2502535000000000001935000000193500d2501735000000243500000027350000002e35033350000001425025350313503535039350
